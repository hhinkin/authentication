# Authentication

Includes:
  - Login
  - Registration
  - User listing

## Instalation

Please clone the repo 
  - Execute ```composer install```
  - Setup user, password and host for database at config/config.php
  - Add salt(hashed string) at config/config.php if not it will be added automatically
  - Add secret_key(hashed string) at config/config.php if not it will be added automatically

You can find SQL file is in folder database.

## Server
  - PHP: php -S 0.0.0.0:8000 -t public/
  - Apache and NGINX: You have to create virtual hosts directed to ```public/``` directory.
  
## Tests
  - Execute ```composer test```