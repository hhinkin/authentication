CREATE SCHEMA `auth` DEFAULT CHARACTER SET utf8;
CREATE TABLE `auth`.`users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `auth`.`users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

ALTER TABLE `auth`.`users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `auth`.`remembered_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `expires_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `auth`.`remembered_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expires_at` (`expires_at`),
  ADD KEY `token` (`token`) USING BTREE;

ALTER TABLE `auth`.`remembered_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

