<?php

return [
    'salt' => '',
    'secret_key' => '',
    'session' =>
        [
            'field_logged_in' => 'auth_logged_in',
            'field_user_id' => 'auth_user_id',
            'field_email' => 'auth_email',
            'field_username' => 'auth_username',
            'field_first_name' => 'auth_user_first_name',
            'field_last_name' => 'auth_user_last_name',
        ],
    'remember_me_duration' => 31536000,
    'remember_me_prefix' => 'auth_cookie',
    'database' => [
        'dbname' => '',
        'user' => '',
        'password' => '',
        'host' => '',
        'driver' => 'pdo_mysql',
    ]
];
