<?php

return [
    [
        'httpMethod' => 'GET',
        'route' => '/',
        'handler' => [
            'class' => 'User\Controller\UserController',
            'method' => 'index'
        ]
    ],
    [
        'httpMethod' => [
            'GET',
            'POST'
        ],
        'route' => '/register',
        'handler' => [
            'class' => 'User\Controller\UserController',
            'method' => 'register'
        ]
    ],
    [
        'httpMethod' => [
            'GET',
            'POST'
        ],
        'route' => '/login',
        'handler' => [
            'class' => 'User\Controller\AuthController',
            'method' => 'login'
        ]
    ],
    [
        'httpMethod' => [
            'POST'
        ],
        'route' => '/logout',
        'handler' => [
            'class' => 'User\Controller\AuthController',
            'method' => 'logout'
        ]
    ],
    [
        'httpMethod' => [
            'GET'
        ],
        'route' => '/notfound',
        'handler' => [
            'class' => 'User\Controller\BaseController',
            'method' => 'notFound'
        ]
    ],
    [
        'httpMethod' => [
            'GET'
        ],
        'route' => '/notallowed',
        'handler' => [
            'class' => 'User\Controller\BaseController',
            'method' => 'notAllowed'
        ]
    ]
];