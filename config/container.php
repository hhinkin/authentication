<?php

use function DI\create;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use User\Interfaces\UserRepositoryInterface;
use User\Repository\UserRepository;
use User\Repository\UserRememberedRepository;
use User\Interfaces\UserRememberedRepositoryInterface;

return [
    UserRepositoryInterface::class => create(UserRepository::class),
    UserRememberedRepositoryInterface::class => create(UserRememberedRepository::class),
    Environment::class => function () {
        $loader = new FilesystemLoader(__DIR__ . '/../src/View');
        return new Environment($loader);
    },
];
