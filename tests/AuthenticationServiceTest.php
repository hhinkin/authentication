<?php

use PHPUnit\Framework\TestCase;
use User\Service\AuthenticationService;

class AuthenticationServiceTest extends TestCase
{
    use \phpmock\phpunit\PHPMock;

    private $repoMock;
    private $rememberRepoMock;

    public function setUp()
    {
        $this->repoMock = $this->getMockBuilder(\User\Interfaces\UserRepositoryInterface::class)
            ->setMethods(['addUser', 'findUserByEmail', 'findAllUsers'])
            ->getMock();
        $this->rememberRepoMock = $this->getMockBuilder(\User\Interfaces\UserRememberedRepositoryInterface::class)
            ->setMethods(['findByToken', 'updateToken', 'saveToken'])
            ->getMock();
    }

    public function testAuthenticateUser()
    {
        $response = [
            'success' => true,
            'data' => [
                'id' => 1,
                'password' => '123',
                'email' => 'test@example.com',
                'username' => 'hhinkin',
                'first_name' => 'Hristo',
                'last_name' => 'Hinkin'
            ]
        ];

        $result = [
            'success' => true,
            'userData' => [
                'id' => 1,
                'email' => 'test@example.com',
                'username' => 'hhinkin',
                'first_name' => 'Hristo',
                'last_name' => 'Hinkin'
            ]
        ];

        $sessionMock = $this->getMockBuilder(\Symfony\Component\HttpFoundation\Session\Session::class)
            ->setMethods(['get', 'set', 'clear'])
            ->getMock();

        $this->repoMock->expects($this->any())
            ->method('addUser');
        $this->repoMock->expects($this->once())
            ->method('findUserByEmail')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findAllUsers');

        $this->rememberRepoMock->expects($this->any())
            ->method('findByToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('updateToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('saveToken');

        $passwordVerify = $this->getFunctionMock('User\Service', 'password_verify');
        $passwordVerify->expects($this->once())->willReturn(true);

        $authService = new AuthenticationService($sessionMock, $this->repoMock, $this->rememberRepoMock);
        $email = 'test@example.com';
        $password = '123';
        $expected = $authService->authenticate($email, $password);

        $this->assertEquals($expected, $result);
    }

    public function testAuthenticateUserNotFound()
    {
        $response = [
            'success' => true,
            'data' => []
        ];

        $result = [
            'success' => false,
            'validationErrors' => ['User not found!']
        ];

        $sessionMock = $this->getMockBuilder(\Symfony\Component\HttpFoundation\Session\Session::class)
            ->setMethods(['get', 'set', 'clear'])
            ->getMock();

        $this->repoMock->expects($this->any())
            ->method('addUser');
        $this->repoMock->expects($this->once())
            ->method('findUserByEmail')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findAllUsers');

        $this->rememberRepoMock->expects($this->any())
            ->method('findByToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('updateToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('saveToken');

        $authService = new AuthenticationService($sessionMock, $this->repoMock, $this->rememberRepoMock);
        $email = 'test@example.com';
        $password = '123';
        $expected = $authService->authenticate($email, $password);
        //var_dump($expected);die;
        $this->assertEquals($expected, $result);
    }

    public function testAuthenticateWrongPassword()
    {
        $response = [
            'success' => true,
            'data' => [
                'id' => 1,
                'password' => '123',
                'email' => 'test@example.com',
                'username' => 'hhinkin',
                'first_name' => 'Hristo',
                'last_name' => 'Hinkin'
            ]
        ];

        $result = [
            'success' => false,
            'validationErrors' => ['Wrong email or password!']
        ];

        $sessionMock = $this->getMockBuilder(\Symfony\Component\HttpFoundation\Session\Session::class)
            ->setMethods(['get', 'set', 'clear'])
            ->getMock();

        $this->repoMock->expects($this->any())
            ->method('addUser');
        $this->repoMock->expects($this->once())
            ->method('findUserByEmail')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findAllUsers');

        $this->rememberRepoMock->expects($this->any())
            ->method('findByToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('updateToken');
        $this->rememberRepoMock->expects($this->any())
            ->method('saveToken');

        $authService = new AuthenticationService($sessionMock, $this->repoMock, $this->rememberRepoMock);
        $email = 'test@example.com';
        $password = '123';
        $expected = $authService->authenticate($email, $password);

        $this->assertEquals($expected, $result);
    }
}