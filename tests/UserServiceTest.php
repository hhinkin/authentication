<?php

use PHPUnit\Framework\TestCase;
use User\Service\UserService;

class UserServiceTest extends TestCase
{
    private $repoMock;

    public function setUp()
    {
        $this->repoMock = $this->getMockBuilder(\User\Interfaces\UserRepositoryInterface::class)
            ->setMethods(['addUser', 'findUserByEmail', 'findAllUsers'])
            ->getMock();
    }

    public function testCreateUser()
    {
        $response = [
            'success' => true,
            'id' => 1
        ];

        $this->repoMock->expects($this->once())
            ->method('addUser')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findUserByEmail');
        $this->repoMock->expects($this->any())
            ->method('findAllUsers');

        $userService = new UserService($this->repoMock);
        $email = 'hhinkin@gmail.com';
        $password = '123';
        $result = $userService->createUser($email, $password, '', '', '');

        $this->assertEquals($result, $response);
    }

    public function testCreateUserDatabaseError()
    {
        $response = ['Something went wrong the record was not saved!'];

        $this->repoMock->expects($this->once())
            ->method('addUser')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findUserByEmail');
        $this->repoMock->expects($this->any())
            ->method('findAllUsers');

        $userService = new UserService($this->repoMock);
        $email = 'hhinkin@gmail.com';
        $password = '123';
        $result = $userService->createUser($email, $password, '', '', '');

        $this->assertEquals($result, $response);
    }

    public function testGetUserListing()
    {
        $response = [
            'success' => true,
            'id' => 1
        ];

        $this->repoMock->expects($this->any())
            ->method('addUser');
        $this->repoMock->expects($this->any())
            ->method('findUserByEmail');
        $this->repoMock->expects($this->once())
            ->method('findAllUsers')
            ->willReturn($response);

        $userService = new UserService($this->repoMock);
        $result = $userService->getUserListing();

        $this->assertEquals($result, $response);
    }

    public function testGetUserListingDatabaseError()
    {
        $response = ['Something went wrong the record was not saved!'];
        $this->repoMock->expects($this->any())
            ->method('addUser')
            ->willReturn($response);
        $this->repoMock->expects($this->any())
            ->method('findUserByEmail')
            ->willReturn($response);
        $this->repoMock->expects($this->once())
            ->method('findAllUsers')
            ->willReturn($response);

        $userService = new UserService($this->repoMock);
        $result = $userService->getUserListing();

        $this->assertEquals($result, $response);
    }
}