<?php

use FastRoute\RouteCollector;
use User\Helper\FileHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;

$container = require __DIR__ . '/container.php';

$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {
    $routes = require __DIR__ . '/../config/routes.php';
    foreach ($routes as $route) {
        $r->addRoute(
            $route['httpMethod'],
            $route['route'],
            [
                $route['handler']['class'],
                $route['handler']['method']
            ]
        );
    }
});

$route = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
$match = array_shift($route);

switch ($match) {
    case FastRoute\Dispatcher::NOT_FOUND:
        $response = new RedirectResponse('/notfound');
        return $response->send();
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response = new RedirectResponse('/notallowed');
        return $response->send();
        break;

    case FastRoute\Dispatcher::FOUND:
        FileHelper::setHash();
        list($controller, $parameters) = $route;
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $container->call($controller, $parameters);
        break;
}
