<?php

namespace User\Controller;

use Twig_Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use User\Service\AuthenticationService;
use User\Validator\UserValidator;

/**
 * Class AuthController
 * @package User\Controller
 */
class AuthController extends BaseController
{
    /**
     * @var UserValidator
     */
    private $validator;

    /**
     * AuthController constructor.
     * @param AuthenticationService $service
     * @param Twig_Environment $twig
     * @param Request $request
     * @param Response $response
     * @param UserValidator $validator
     */
    public function __construct(
        AuthenticationService $service,
        Twig_Environment $twig,
        Request $request,
        Response $response,
        UserValidator $validator
    )
    {
        parent::__construct($service, $twig, $request, $response);
        $this->validator = $validator;
    }

    /**
     * Authenticates user and redirects to home page
     *
     * @return \User\Traits\html
     */
    public function login()
    {
        $data = [];
        if ($this->service->isLoggedIn()){
            $this->redirect->send();
        }
        if ($this->request->getMethod() == Request::METHOD_POST) {
            $email = $this->request->get('email');
            $password = $this->request->get('password');
            $remember = $this->request->get('remember');
            $data['validationErrors'] = $this->validator->validateLogin($email, $password);

            if (empty($data['validationErrors'])) {
                $data = $this->service->authenticate($email, $password);
                if (!empty($data['success']) && !empty($remember)) {
                    $this->service->createRememberToken($this->redirect, $data['userData']['id']);
                    $this->redirect->send();
                }
            }
        }

        return $this->render('login.twig', $data);
    }

    /**
     * Destroy user session
     */
    public function logout()
    {
        $this->service->logout($this->redirect);
        $this->redirect->setTargetUrl('/login');
        $this->redirect->send();
    }
}
