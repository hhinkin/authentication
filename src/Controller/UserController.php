<?php

namespace User\Controller;

use Twig_Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use User\Service\AuthenticationService;
use User\Validator\UserValidator;
use User\Service\UserService;

/**
 * Class UserController
 * @package User\Controller
 */
class UserController extends BaseController
{
    /**
     * @var UserValidator
     */
    private $validator;
    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param AuthenticationService $service
     * @param Twig_Environment $twig
     * @param Request $request
     * @param Response $response
     * @param UserValidator $validator
     * @param UserService $userService
     */
    public function __construct(
        AuthenticationService $service,
        Twig_Environment $twig,
        Request $request,
        Response $response,
        UserValidator $validator,
        UserService $userService
    )
    {
        parent::__construct($service, $twig, $request, $response);
        $this->validator = $validator;
        $this->userService = $userService;
    }

    /**
     * Show registered users listing
     *
     * @return \User\Traits\html
     */
    public function index()
    {
        $data = [];
        if ($this->service->isLoggedIn()) {
            $data['userData'] = $this->service->getUserData();
            $listing = $this->userService->getUserListing();
            if (!empty($listing['success'])) {
                $data['users'] = $listing['data'];
            }
        } else {
            $this->redirect->setTargetUrl('/login');
            $this->redirect->send();
        }

        return $this->render('index.twig', $data);
    }

    /**
     * Creates new user
     *
     * @return \User\Traits\html
     */
    public function register()
    {
        $data = [];
        if ($this->service->isLoggedIn()){
            $this->redirect->send();
        }
        if ($this->request->getMethod() == Request::METHOD_POST) {
            $email = $this->request->get('email');
            $password = $this->request->get('password');
            $passwordConfirm = $this->request->get('password_confirm');
            $username = $this->request->get('username') ?? '';
            $firstName = $this->request->get('first_name') ?? '';
            $lastName = $this->request->get('last_name') ?? '';
            $data['validationErrors'] = $this->validator->validateRegistration($email, $password, $passwordConfirm);

            if (empty($data['validationErrors'])) {
                $result = $this->userService->createUser($email, $password, $username, $firstName, $lastName);
                if (!empty($result['success'])) {
                    $this->service->setUserData($result['id'], $email, $username, $firstName, $lastName);
                    $this->redirect->send();
                } else {
                    $data['validationErrors'] = $result['validationErrors'];
                }
            }
        }

        return $this->render('register.twig', $data);
    }
}
