<?php

namespace User\Controller;

use Twig_Environment;
use User\Traits\ControllerTrait;
use User\Service\AuthenticationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class BaseController
 * @package User\Controller
 */
class BaseController
{
    use ControllerTrait;

    /**
     * @var AuthenticationService
     */
    protected $service;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var RedirectResponse
     */
    protected $redirect;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * Creates nessesery instancies
     *
     * BaseController constructor.
     * @param AuthenticationService $service
     * @param Twig_Environment $twig
     * @param Request $request
     * @param Response $response
     */
    public function __construct(
        AuthenticationService $service,
        Twig_Environment $twig,
        Request $request,
        Response $response
    )
    {
        $this->request = $request::createFromGlobals();
        $this->response = $response;
        $this->service = $service;
        $this->twig = $twig;
        $this->redirect = $redirect = new RedirectResponse('/');
        $this->service->processRememberMe($this->request, $this->response);
    }

    /**
     * Handle 404 eror
     *
     * @return \User\Traits\html
     */
    public function notFound()
    {
        return $this->render('error-404.twig', []);
    }

    /**
     * Handle 405 error
     *
     * @return \User\Traits\html
     */
    public function notAllowed()
    {
        return $this->render('error-405.twig', []);
    }
}