<?php

namespace User\Validator;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints as Assert;
use User\Repository\UserRepository;

class UserValidator
{
    const TAKEN_EMAIL = 'Email already in use!';
    const PASSWORDS_NOT_MATCH = 'The passwords do not match';

    /**
     * @var \Symfony\Component\Validator\Validator\ValidatorInterface
     */
    private $validator;
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * Creates Validation instance
     * Injects UserRepository
     *
     * UserValidator constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->validator = Validation::createValidator();
        $this->userRepo = $userRepo;
    }

    /**
     * Validates login email and password
     *
     * @param string $email
     * @param string $password
     * @return array
     */
    public function validateLogin($email,$password)
    {
        $emailValidation = $this->validateEmail($email);
        $passwordValidation = $this->validatePassword($password);

        return array_merge($emailValidation, $passwordValidation);
    }

    /**
     * Validates registartion email is valid email in use and password
     *
     * @param string $email
     * @param string $password
     * @param string $passwordConfirm
     * @return array
     */
    public function validateRegistration(string $email, string $password, string $passwordConfirm)
    {
        $user = $this->userRepo->findUserByEmail($email);
        $uniqueValidation = !empty($user['data']) ? [self::TAKEN_EMAIL] : [];
        $passwordMatchValidation = $password != $passwordConfirm ? self::PASSWORDS_NOT_MATCH : [];
        $emailValidation = $this->validateEmail($email);
        $passwordValidation = $this->validatePassword($password);

        return array_merge($uniqueValidation, $passwordMatchValidation, $emailValidation, $passwordValidation);
    }

    /**
     * Checks email required, not blank and valid email
     *
     * @param string $email
     * @return array
     */
    private function validateEmail($email)
    {
        $errors = [];
        $violations = $this->validator->validate($email, [
            new NotNull(),
            new NotBlank(),
            new Required(),
            new Email()
        ]);

        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                $errors[] = 'Email: ' . $violation->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Checks password required, length and is not blank
     *
     * @param string $password
     * @return array
     */
    private function validatePassword($password)
    {
        $errors = [];
        $violations = $this->validator->validate($password, [
            new Assert\Type('alnum'),
            new NotNull(),
            new NotBlank(),
            new Required(),
            new Length(['min' => 6])
        ]);

        if (count($violations) !== 0) {
            foreach ($violations as $violation) {
                $errors[] = 'Password: ' . $violation->getMessage();
            }
        }

        return $errors;
    }
}
