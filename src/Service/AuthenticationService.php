<?php

namespace User\Service;

use User\Helper\FileHelper;
use User\Interfaces\AuthenticationInterface;
use User\Interfaces\UserRepositoryInterface;
use User\Interfaces\UserRememberedRepositoryInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class AuthenticationService implements AuthenticationInterface
{
    const WRONG_EMAIL_OR_PASSWORD = 'Wrong email or password!';
    const USER_NOT_FOUND = 'User not found!';
    const ENCRYPT_METHOD = 'AES-256-CBC';
    const HASH_METHOD = 'sha256';
    const ENCRYPT_ACTION = 'encrypt';
    const DECRYPT_ACTION = 'decrypt';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var UserRememberedRepositoryInterface
     */
    private $userRememberRepo;

    /**
     * Creates new instances of Session, UserRepository and UserRememberedRepository
     *
     * AuthenticationService constructor.
     * @param Session $session
     * @param UserRepositoryInterface $userRepo
     * @param UserRememberedRepositoryInterface $userRememberRepo
     */
    public function __construct(Session $session, UserRepositoryInterface $userRepo, UserRememberedRepositoryInterface $userRememberRepo)
    {
        $this->session = $session;
        $this->userRepo = $userRepo;
        $this->userRememberRepo = $userRememberRepo;
    }

    /**
     * If present user authenticates
     *
     * @param string $email
     * @param string $password
     * @return array
     */
    public function authenticate(string $email, string $password)
    {
        $data = [
            'success' => false
        ];

        $user = $this->userRepo->findUserByEmail($email);
        switch (true) {
            // If user not found returns error
            case (empty($user['data'])):
                $data['validationErrors'] = [self::USER_NOT_FOUND];
                break;
            // User is found checks his password
            case (!empty($user['data']) && password_verify($password, $user['data']['password'])):
                unset($user['data']['password']);
                $user = $user['data'];
                $this->setUserData(
                    $user['id'],
                    $user['email'],
                    $user['username'],
                    $user['first_name'],
                    $user['last_name']
                );
                $data['success'] = true;
                $data['userData'] = $user;
                break;
            // User not found or password is not valid
            default:
                $data['validationErrors'] = [self::WRONG_EMAIL_OR_PASSWORD];
        }

        return $data;
    }

    /**
     * Checks session field loggedin is present
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $loggedIn = false;
        if (!empty($this->session->get($config['session']['field_logged_in']))) {
            $loggedIn = true;
        }

        return $loggedIn;
    }

    /**
     * User is authenticated set his data in session
     *
     * @param int $userId
     * @param string $email
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     * @return void
     */
    public function setUserData(int $userId, string $email, string $username = '', string $firstName = '', string $lastName = '')
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $sessionConfig = $config['session'];
        $this->session->set($sessionConfig['field_logged_in'], true);
        $this->session->set($sessionConfig['field_user_id'], (int)$userId);
        $this->session->set($sessionConfig['field_email'], $email);
        $this->session->set($sessionConfig['field_username'], $username);
        $this->session->set($sessionConfig['field_first_name'], $firstName);
        $this->session->set($sessionConfig['field_last_name'], $lastName);
    }

    /**
     * If user is logged in return his data
     *
     * @return array
     */
    public function getUserData()
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $sessionConfig = $config['session'];

        $userData = [
            'userId' => $this->session->get($sessionConfig['field_user_id']),
            'email' => $this->session->get($sessionConfig['field_email']),
            'userName' => $this->session->get($sessionConfig['field_username']),
            'firstName' => $this->session->get($sessionConfig['field_first_name']),
            'lastName' => $this->session->get($sessionConfig['field_last_name']),
        ];

        return $userData;
    }

    /**
     * Creates remember token store or update it in database
     *
     * @param Response $response
     * @param int $userId
     * @param string $oldToken
     */
    public function createRememberToken(Response $response, int $userId, string $oldToken = '')
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $token = FileHelper::generateSalt();
        $tokenHashed = password_hash($token, PASSWORD_DEFAULT);
        $expires = time() + $config['remember_me_duration'];

        if (empty($oldToken)) {
            $cookie = $this->userRememberRepo->saveToken($userId, $tokenHashed, $expires);
        } else {
            $cookie = $this->userRememberRepo->updateToken($oldToken, $tokenHashed);
        }

        if (!empty($cookie['success'])) {
            $name = $this->createCookieName();
            $tokenHashed = $this->encryptDecryptCookie($tokenHashed, self::ENCRYPT_ACTION);
            $response->headers->setCookie(Cookie::create($name, $tokenHashed, $expires));
        }
    }

    /**
     * Creates hashed cookie name
     *
     * @return string
     */
    public static function createCookieName()
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $cookieName = md5($config['remember_me_prefix'] . '_' . $config['salt']);

        return $cookieName;
    }

    /**
     * Destroys user session and removes remember me cookie
     *
     * @param Response $response
     */
    public function logout(Response $response)
    {
        $this->session->clear();
        $cookieName = $this->createCookieName();
        $response->headers->clearCookie($cookieName);
    }

    /**
     * If session is over checks for remember me cookie anf if it presists
     *
     * @param Request $request
     * @param Response $response
     */
    public function processRememberMe(Request $request, Response $response)
    {
        if (!$this->isLoggedIn()) {
            $cookieName = $this->createCookieName();
            $token = $request->cookies->get($cookieName);
            if (!empty($token)) {
                $token = $this->encryptDecryptCookie($token, self::DECRYPT_ACTION);
                $userData = $this->userRememberRepo->findByToken($token);
                if (!empty($userData['data'])) {
                    $this->setUserData(
                        $userData['data']['id'],
                        $userData['data']['email'],
                        $userData['data']['username'],
                        $userData['data']['first_name'],
                        $userData['data']['last_name']
                    );
                    $this->createRememberToken($response, $userData['data']['id'], $userData['data']['token']);
                }
            }
        }
    }

    /**
     * Two way string encryption
     *
     * @param string $string
     * @param string $action
     * @return string
     */
    public function encryptDecryptCookie(string $string, string $action)
    {
        $encryptMethod = self::ENCRYPT_METHOD;
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        $secretKey = $config['secret_key'];
        $salt = $config['salt'];
        $key = hash(self::HASH_METHOD, $secretKey);
        $initVector = substr(hash(self::HASH_METHOD, $salt), 0, 16);

        if ($action == self::ENCRYPT_ACTION) {
            $output = openssl_encrypt($string, $encryptMethod, $key, 0, $initVector);
            $output = base64_encode($output);
        } else {
            $output = openssl_decrypt(base64_decode($string), $encryptMethod, $key, 0, $initVector);
        }

        return $output;
    }
}
