<?php

namespace User\Service;

use User\Interfaces\UserRepositoryInterface;


class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * Injects UserRepository binded via DI container with UserRepositoryInterface
     *
     * UserService constructor.
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Creates new user
     *
     * @param string $email
     * @param string $password
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     * @return mixed
     */
    public function createUser(string $email, string $password, string $username = '', string $firstName = '', string $lastName = '')
    {
        return $this->userRepo->addUser($email, password_hash($password, PASSWORD_DEFAULT), $username, $firstName, $lastName);
    }

    /**
     * Returns listing of all registered users
     *
     * @return array
     */
    public function getUserListing()
    {
        return $this->userRepo->findAllUsers();
    }
}
