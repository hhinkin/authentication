<?php

namespace User\Traits;

trait ControllerTrait
{
    /**
     * Creates easy way to create controller response
     *
     * @param string $view
     * @param array $params
     * @return html
     */
    public function render(string $view, array $params = [])
    {
         return $this->response->setContent($this->twig->render($view, $params))->send();
    }
}
