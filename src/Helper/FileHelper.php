<?php


namespace User\Helper;


class FileHelper
{
    const GONFIG_PATH = __DIR__ . '/../../config/';
    const MAIN_CONFIG_FILE = 'config';
    const RANDOM_STRING_LENGTH = 24;

    /**
     * Returns config file
     *
     * @param string $file
     * @return mixed
     */
    public static function getConfig(string $file)
    {
        $path = self::GONFIG_PATH . $file;

        return self::getFile($path);
    }

    /**
     * Require file by given path
     *
     * @param string $path
     * @return mixed
     */
    public static function getFile(string $path)
    {
        try {
            if (is_file($path . '.php')) {
                $file = require $path . '.php';
            } else {
                throw new \Exception('File do not exist');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }

        return $file;
    }

    /**
     * Create application hashed secred word and writes it to config file
     */
    public static function setHash()
    {
        $config = self::getConfig(self::MAIN_CONFIG_FILE);
        if (empty($config['salt']) || empty($config['secret_key'])) {
            $config['salt'] = $config['salt'] ? $config['salt'] : self::generateSalt();
            $config['secret_key'] = $config['secret_key'] ? $config['secret_key'] : self::generateSalt();
            file_put_contents(self::GONFIG_PATH . self::MAIN_CONFIG_FILE . '.php', '<?php return ' . var_export($config, true) . ';');
        }
    }

    /**
     * Generates hashed string
     *
     * @return string
     */
    public static function generateSalt()
    {
        return md5(openssl_random_pseudo_bytes(self::RANDOM_STRING_LENGTH));
    }
}
