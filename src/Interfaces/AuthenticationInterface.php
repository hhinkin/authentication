<?php

namespace User\Interfaces;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface AuthenticationInterface
 * @package User\Interfaces
 */
interface AuthenticationInterface
{
    /**
     * Enshures that classes that make authentication will have authenticate method
     *
     * @param string $email
     * @param string $password
     * @return mixed
     */
    public function authenticate(string $email, string $password);
}
