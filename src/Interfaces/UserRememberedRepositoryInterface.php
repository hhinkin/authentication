<?php

namespace User\Interfaces;

/**
 * Interface UserRememberedRepositoryInterface
 * @package User\Interfaces
 */
interface UserRememberedRepositoryInterface
{

    /**
     * Finds user by hashed token
     *
     * @param string $token
     * @return array
     */
    public function findByToken(string $token);

    /**
     * Updates token on session expire
     *
     * @param string $token
     * @param string $newToken
     * @return array
     */
    public function updateToken(string $token, string $newToken);
}
