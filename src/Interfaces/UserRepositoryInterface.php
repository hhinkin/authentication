<?php

namespace User\Interfaces;

/**
 * Interface UserRepositoryInterface
 * @package User\Interfaces
 */
interface UserRepositoryInterface
{

    /**
     * On login
     *
     * @param string $email
     * @return mixed
     */
    public function findUserByEmail(string $email);

    /**
     * User listing
     *
     * @return array
     */
    public function findAllUsers();

    /**
     * Registration
     *
     * @param string $email
     * @param string $password
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     * @return mixed
     */
    public function addUser(string $email, string $password, string $username = '', string $firstName = '', string $lastName = '');
}
