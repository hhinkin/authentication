<?php

namespace User\repository;

use User\Helper\FileHelper;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

/**
 * Class only creates database connection
 *
 * Class AbstractRepository
 * @package User\repository
 */
abstract class AbstractRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * AbstractRepository constructor.
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct()
    {
        try {
            $dbConfig = new Configuration();
            $connectionParams = $this->getConnectionCredentials();
            $this->connection = DriverManager::getConnection($connectionParams, $dbConfig);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }

    }

    /**
     * Checks for database credentials
     *
     * @return mixed
     * @throws \Exception
     */
    private function getConnectionCredentials()
    {
        $config = FileHelper::getConfig(FileHelper::MAIN_CONFIG_FILE);
        foreach ($config['database'] as $value) {
            if (empty($value)) {
                throw new \Exception('Connection with database is impossible. Please fill database details in /config/config.php!');
            }
        }

        return $config['database'];
    }
}
