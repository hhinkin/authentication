<?php

namespace User\Repository;

use User\Interfaces\UserRememberedRepositoryInterface;

/**
 * Class UserRememberedRepository
 * @package User\Repository
 */
class UserRememberedRepository extends AbstractRepository implements UserRememberedRepositoryInterface
{

    /**
     * Creates connection using parent __construstor()
     *
     * UserRememberedRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Fins user by cookie token
     *
     * @param string $token
     * @return array
     */
    public function findByToken(string $token)
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->select('ru.token', 'u.id', 'u.email', 'u.username', 'u.first_name', 'u.last_name')
                ->from('remembered_users', 'ru')
                ->innerJoin('ru', 'users', 'u', 'u.id = ru.user_id')
                ->where('token = :token')
                ->andWhere('expires_at > :expires_at')
                ->setParameter(':token', $token)
                ->setParameter(':expires_at', time());

            $statement = $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $data = $statement->fetchAll();
            $data = !empty($data) ? reset($data) : $data;
            $result = [
                'success' => true,
                'data' => $data
            ];
        } catch (\Exception $e) {
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }

    /**
     * When session is over updates token with new one
     *
     * @param string $token
     * @param string $newToken
     * @return array
     */
    public function updateToken(string $token, string $newToken)
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->update('remembered_users')
                ->set('token', ':new_token')
                ->set('updated_at', ':updated_at')
                ->where('token = :token')
                ->setParameter(':new_token', $newToken)
                ->setParameter(':token', $token)
                ->setParameter(':updated_at', time())
            ;

            $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $result = [
                'success' => true
            ];
        } catch (\Exception $e) {
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }

    /**
     * If user checks remember me creates new token
     *
     * @param int $userId
     * @param string $tokenHashed
     * @param int $expires
     * @return array
     */
    public function saveToken(int $userId, string $tokenHashed, int $expires)
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->insert('remembered_users')->values(
                    [
                        'user_id' => ':user_id',
                        'token' => ':token',
                        'expires_at' => ':expires_at',
                        'created_at' => ':created_at',
                        'updated_at' => ':updated_at',
                    ])
                ->setParameter(':user_id', $userId)
                ->setParameter(':token', $tokenHashed)
                ->setParameter(':expires_at', $expires)
                ->setParameter(':updated_at', time())
                ->setParameter(':created_at', time());

            $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $id = $this->connection->lastInsertId();
            $result = [
                'success' => true,
                'id' => $id
            ];
        } catch (\Exception $e) {
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }
}
