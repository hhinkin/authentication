<?php

namespace User\Repository;

use User\Interfaces\UserRepositoryInterface;

/**
 * Class UserRepository
 * @package User\Repository
 */
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     *
     */
    const DATABASE_ERROR_MESSAGE = 'Something went wrong the record was not saved!';

    /**
     * Creates connection using parent __construstor()
     *
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Inserts new user
     *
     * @param string $email
     * @param string $password
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     * @return array
     */
    public function addUser(string $email, string $password, string $username = '', string $firstName = '', string $lastName = '')
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->insert('users')->values(
                    [
                        'username' => ':username',
                        'password' => ':password',
                        'email' => ':email',
                        'first_name' => ':first_name',
                        'last_name' => ':last_name',
                        'created_at' => ':created_at',
                        'updated_at' => ':updated_at',
                    ])
                ->setParameter(':username', $username)
                ->setParameter(':password', $password)
                ->setParameter(':email', $email)
                ->setParameter(':first_name', $firstName)
                ->setParameter(':last_name', $lastName)
                ->setParameter(':updated_at', time())
                ->setParameter(':created_at', time());

            $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $id = $this->connection->lastInsertId();
            $result = [
                'success' => true,
                'id' => $id
            ];
        } catch (\Exception $e) {
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return array
     */
    public function findUserByEmail(string $email)
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->select('id', 'username', 'password', 'email', 'first_name', 'last_name', 'created_at', 'updated_at')
                ->from('users')
                ->where('email = :email')
                ->setMaxResults(1)
                ->setParameter(':email', $email);

            $statement = $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $data = $statement->fetchAll();
            $data = !empty($data) ? reset($data) : $data;
            $result = [
                'success' => true,
                'data' => $data
            ];
        } catch (\Exception $e) {
            // Better will be to log message
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }

    /**
     * Gets list of all registered users
     *
     * @param int $email
     * @return array
     */
    public function findAllUsers()
    {
        $result = [
            'success' => false
        ];
        try {
            $queryBuilder = $this->connection->createQueryBuilder();
            $queryBuilder
                ->select('id', 'username', 'email', 'first_name', 'last_name', 'created_at')
                ->from('users');

            $statement = $this->connection->executeQuery($queryBuilder->getSQL(), $queryBuilder->getParameters(), $queryBuilder->getParameterTypes());
            $data = $statement->fetchAll();

            $result = [
                'success' => true,
                'data' => $data
            ];
        } catch (\Exception $e) {
            // Better will be to log message
            $result['validationErrors'][] = [self::DATABASE_ERROR_MESSAGE];
        }

        return $result;
    }
}
