<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!is_file(__DIR__ . '/../vendor/autoload.php')) {
    die('Please run <b>composer install</b> first!');
}

require __DIR__ . '/../app/bootstrap.php';

